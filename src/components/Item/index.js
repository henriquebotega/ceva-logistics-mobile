import React, { useState, useEffect } from "react";
import { View, Text, TextInput, TouchableOpacity } from "react-native";
import Icon from "react-native-vector-icons/MaterialIcons";

import styles from "./styles";

const Item = ({
	d,
	delDoor,
	changeMinus,
	changePlus,
	changePallet,
	changeDoor,
	vDoor,
	vPallet,
}) => {
	const [nDoor, setNDoor] = useState( 0);
	const [nPallet, setNPallet] = useState("");

	useEffect(() => {
		setNDoor(vDoor)
	}, [vDoor])

	useEffect(() => {
		setNPallet(vPallet)
	}, [vPallet])

	return (
		<View style={styles.currentDoor}>
			<View style={styles.containerItem}>
				<Text>DEL</Text>
				<TouchableOpacity onPress={() => delDoor(d)}>
					<Icon name="cancel" size={45} color="#990000" />
				</TouchableOpacity>
			</View>

			<View style={styles.containerItem}>
				<Text>DOOR</Text>
				<TextInput
					style={[styles.field, { width: 45, height: 45, fontSize: 24 }]}
					value={String(nDoor)}
					onChangeText={(t) => setNDoor(t)}
					onBlur={() => changeDoor(d, nDoor)}
					keyboardType={"numeric"}
				/>
			</View>

			<View style={styles.containerItem}>
				<Text>PALLET</Text>
				<TextInput
					style={[styles.field, {  borderRadius: 0, width: 150, height: 50, fontSize: 18 }]}
					value={String(nPallet)}
					onChangeText={(t) => setNPallet(t)}
					onBlur={() => changePallet(d, nPallet)}
					keyboardType={"default"}
				/>
			</View>

			<View style={styles.containerItem}>
				<Text>UNIT</Text>
				<View style={styles.containerQtd}>
					<TouchableOpacity onPress={() => changeMinus(d)}>
						<Icon name="remove-circle" size={45} color="#0000FF" />
					</TouchableOpacity>

					<Text style={styles.btnQtdValue}>
						{d.qtd < 10 ? "0" + d.qtd : d.qtd}
					</Text>

					<TouchableOpacity onPress={() => changePlus(d)}>
						<Icon name="add-circle" size={45} color="#0000FF" />
					</TouchableOpacity>
				</View>
			</View>
		</View>
	);
};

export default Item;
