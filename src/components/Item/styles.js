import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
	currentDoor: {
		flex: 1,
		flexDirection: "row",
		marginTop: 8,
		justifyContent: "space-between",
	},
	containerItem: {
		paddingLeft: 5,
		paddingRight: 5,
		alignItems: "center",
		justifyContent: "space-between",
	},
	field: {
		color: "#FF3366",
		fontWeight: "bold",
		borderRadius: 50,
		borderWidth: 1,
		borderColor: "#FF3366",
		borderStyle: "solid",
		textAlign: "center",
	},
	containerQtd: {
		flexDirection: "row",
		justifyContent: "space-around",
		alignItems: "center",
	},
	btnQtdValue: {
		color: "#FF3366",
		fontSize: 24,
		fontWeight: "bold",
	}
});

export default styles;
