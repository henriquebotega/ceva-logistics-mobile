import axios from "axios";
import io from "socket.io-client";

export const ioURL = "https://ceva-logistics-backend.herokuapp.com";

export const api = axios.create({
	baseURL: ioURL + "/api",
});

export const socket = io(ioURL);
