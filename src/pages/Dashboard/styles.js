import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
	logo: {
		width: 100,
		height: 100,
	},
	modalView: {
		backgroundColor: "#555",
		margin: 10,
		padding: 10,
		flex: 1,
	},
	row: {
		flexDirection: "row",
		justifyContent: "space-between",
		backgroundColor: "#fff",
		margin: 1,
		padding: 5
	},
	rowLeft: {
		color: "#222",
		flex: 1,
		padding: 10,
		fontSize: 18,
	},
	rowRight: {
		color: "#222",
		fontSize: 20,
		padding: 10,
		textAlign: "right",
	},
	content: {
		flex: 1,
		paddingTop: 10,
		paddingBottom: 10,
		justifyContent: "space-between"
	},
	btnClose: {
		backgroundColor: "#fff",
		padding: 10,
		alignItems: "center",
	},
	rowRightBox: {
		flexDirection: "row",
	},
	rowRightBoxTextSuccess: {
		backgroundColor: "#050",
	},
	rowRightBoxText: {
		color: "#fff",
		backgroundColor: "#500",
		padding: 5,
		margin: 5,
	},
	btnCloseText: {
		color: "#222"
	}
});

export default styles;
