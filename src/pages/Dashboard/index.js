import React, { useState, useEffect } from "react";
import {
	Platform,
	SafeAreaView,
	Image,
	View,
	Text,
	TouchableOpacity,
	ScrollView,
	Modal,
} from "react-native";
import CurrencyFormatter from "currency-formatter"
import { api, socket } from "../../services/api";
import Icon from "react-native-vector-icons/MaterialIcons";

import Item from "../../components/Item";
import logo from "../../assets/logo.jpeg";
import styles from "./styles";

const Dashboard = () => {
	const [doors, setDoors] = useState([]);
	const [info, setInfo] = useState({
		total: 14587,
		soFar: 11150,
		lastHour: 4187,
		cpt8: [
			{
				title: "C001",
				total: 7,
				done: 7
			}
		],
		cpt10: [
			{
				title: "D001",
				total: 10,
				done: 1
			},
			{
				title: "B001",
				total: 3,
				done: 3
			},
			{
				title: "A025",
				total: 7,
				done: 3
			}			
		]
	});
	const [modalVisible, setModalVisible] = useState(false);

	useEffect(() => {
		getToDo();
	}, []);

	useEffect(() => {
		if(modalVisible) {
			const nTotal = Math.floor(Math.random() * 30000);
			const nVlr = CurrencyFormatter.format(nTotal, {code: 'USD', symbol: ''})

			setInfo({
				...info, 
				total: nVlr.replace(".00", "")
			})
		}
	}, [modalVisible]);

	useEffect(() => {
		socket.connect();

		socket.on("newToDo", (data) => {
			setDoors(oldData => {
				return [data, ...oldData]
			})
		})

		socket.on("delToDo", (id) => {
			setDoors(oldData => {
				return [...oldData.filter(i => i._id !== id)]
			})
		})

		socket.on("editToDo", (data) => {
			setDoors(oldData => {
				return [...oldData.map(i => {
					return i._id === data._id ? data: i
				})]
			})
		});

		return () => {
			socket.disconnect();
		};
	}, []);

	const getInfo = () => {
		setModalVisible(true);
	}

	const closeModal = () => {
		setModalVisible(!modalVisible)
	}

	async function getToDo() {
		const { data } = await api.get("/todo");
		setDoors(data);
	}

	const saveTodo = async (d) => {
		if (d._id) {
			await api.put(`/todo/${d._id}`, d);
		} else {
			await api.post("/todo", d);
		}
	};

	const newDoor = async () => {
		const d = { door: 0, pallet: "", qtd: 0 };
		saveTodo(d);
	};

	const delDoor = async (d) => {
		await api.delete("/todo/" + d._id);
	};

	const changeDoor = async (d, door) => {
		d.door = door;
		saveTodo(d);
	};

	const changePallet = async (d, pallet) => {
		d.pallet = pallet;
		saveTodo(d);
	};

	const changeMinus = async (d) => {
		d.qtd = d.qtd - 1;
		saveTodo(d);
	};

	const changePlus = async (d) => {
		d.qtd = d.qtd + 1;
		saveTodo(d);
	};

	return (
		<SafeAreaView
			style={{
				flex: 1,
				alignItems: "center",
				justifyContent: "center",
				paddingTop: Platform.OS === "android" ? 25 : 0,
				paddingBottom: Platform.OS === "android" ? 25 : 0,
			}}
		>
			<Modal visible={modalVisible} transparent={true} animationType={"fade"}>
                <View style={styles.modalView}>
					<View style={styles.content}>
						<View>
							<View style={styles.row}>
								<Text style={styles.rowLeft}>Total of the day</Text>
								<Text style={styles.rowRight}>{info.total}</Text>
							</View>

							<View style={styles.row}>
								<Text style={styles.rowLeft}>Scanned so far</Text>
								<Text style={styles.rowRight}>{info.soFar}</Text>
							</View>

							<View style={styles.row}>
								<Text style={styles.rowLeft}>Scanned in the last hour</Text>
								<Text style={styles.rowRight}>{info.lastHour}</Text>
							</View>
						</View>
	
						<View>
							<View style={styles.row}>
								<Text style={styles.rowLeft}>CPT 8pm</Text>
								<View style={styles.rowRightBox}>
									{info.cpt8.map((obj, i) => {
											const vlr = obj.total - obj.done;
											const stylesSuccess = vlr === 0 ? styles.rowRightBoxTextSuccess : null
											return <Text key={i} style={[styles.rowRightBoxText, stylesSuccess]}>{obj.title}</Text>
										}
									)}
								</View>
							</View>

							<View style={styles.row}>
								<Text style={styles.rowLeft}>CPT 10pm</Text>
								<View style={styles.rowRightBox}>
									{info.cpt10.map((obj, i) => {
											const vlr = obj.total - obj.done;
											const stylesSuccess = vlr === 0 ? styles.rowRightBoxTextSuccess : null
											return <Text key={i} style={[styles.rowRightBoxText, stylesSuccess]}>{`${obj.title} ${vlr === 0 ? "" : `(${vlr})`}`}</Text>
										}
									)}
								</View>
							</View>
						</View>
					</View>

					<TouchableOpacity onPress={closeModal} style={styles.btnClose}>
						<Text style={styles.btnCloseText}>Close</Text>
					</TouchableOpacity>
				</View>
			</Modal>

			<View style={{ position: "absolute", left: 15, top: 50 }}>
				<TouchableOpacity onPress={() => getToDo()}>
					<Icon name="refresh" size={40} color="green" />
				</TouchableOpacity>
			</View>

			<View style={{ position: "absolute", left: 90, top: 50 }}>
				<TouchableOpacity onPress={() => getInfo()}>
					<Icon name="info" size={40} color="green" />
				</TouchableOpacity>
			</View>

			<Image source={logo} style={styles.logo} />

			<View style={{ position: "absolute", right: 15, top: 50 }}>
				<TouchableOpacity onPress={() => newDoor()}>
					<Icon name="add" size={40} color="green" />
				</TouchableOpacity>
			</View>

			<ScrollView>
				{doors.map((i) => {
					return (
						<Item
							key={i._id}
							d={i}
							delDoor={delDoor}
							changeMinus={changeMinus}
							changePlus={changePlus}
							changeDoor={changeDoor}
							changePallet={changePallet}
							vDoor={i.door}
							vPallet={i.pallet}
						/>
					);
				})}
			</ScrollView>
		</SafeAreaView>
	);
};

export default Dashboard;
